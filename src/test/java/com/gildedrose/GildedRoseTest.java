package com.gildedrose;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GildedRoseTest {

    private static final String GOLDEN_MASTER =
        "-------- day 0 --------" +
        "+5 Dexterity Vest, 10, 20" +
        "+5 Dexterity Vest, 0, 20" +
        "+5 Dexterity Vest, 1, 1" +
        "Aged Brie, 2, 0" +
        "Aged Brie, 2, 49" +
        "Aged Brie, 0, 0" +
        "Sulfuras, Hand of Ragnaros, 0, 80" +
        "Sulfuras, Hand of Ragnaros, 20, 40" +
        "Sulfuras, Hand of Ragnaros, -1, 40" +
        "Backstage passes to a TAFKAL80ETC concert, 15, 49" +
        "Backstage passes to a TAFKAL80ETC concert, 15, 20" +
        "Backstage passes to a TAFKAL80ETC concert, 10, 20" +
        "Backstage passes to a TAFKAL80ETC concert, 5, 20" +
        "Backstage passes to a TAFKAL80ETC concert, 1, 20" +
        "Conjured Mana Cake, 3, 6" +
        "Conjured Mana Cake, 0, 4" +
        "Conjured Mana Cake, 1, 1" +
        "-------- day 1 --------" +
        "+5 Dexterity Vest, 9, 19" +
        "+5 Dexterity Vest, -1, 18" +
        "+5 Dexterity Vest, 0, 0" +
        "Aged Brie, 1, 1" +
        "Aged Brie, 1, 50" +
        "Aged Brie, -1, 2" +
        "Sulfuras, Hand of Ragnaros, 0, 80" +
        "Sulfuras, Hand of Ragnaros, 20, 40" +
        "Sulfuras, Hand of Ragnaros, -1, 40" +
        "Backstage passes to a TAFKAL80ETC concert, 14, 50" +
        "Backstage passes to a TAFKAL80ETC concert, 14, 21" +
        "Backstage passes to a TAFKAL80ETC concert, 9, 22" +
        "Backstage passes to a TAFKAL80ETC concert, 4, 23" +
        "Backstage passes to a TAFKAL80ETC concert, 0, 23" +
        "Conjured Mana Cake, 2, 4" +
        "Conjured Mana Cake, -1, 0" +
        "Conjured Mana Cake, 0, 0" +
        "-------- day 2 --------" +
        "+5 Dexterity Vest, 8, 18" +
        "+5 Dexterity Vest, -2, 16" +
        "+5 Dexterity Vest, -1, 0" +
        "Aged Brie, 0, 2" +
        "Aged Brie, 0, 50" +
        "Aged Brie, -2, 4" +
        "Sulfuras, Hand of Ragnaros, 0, 80" +
        "Sulfuras, Hand of Ragnaros, 20, 40" +
        "Sulfuras, Hand of Ragnaros, -1, 40" +
        "Backstage passes to a TAFKAL80ETC concert, 13, 50" +
        "Backstage passes to a TAFKAL80ETC concert, 13, 22" +
        "Backstage passes to a TAFKAL80ETC concert, 8, 24" +
        "Backstage passes to a TAFKAL80ETC concert, 3, 26" +
        "Backstage passes to a TAFKAL80ETC concert, -1, 0" +
        "Conjured Mana Cake, 1, 2" +
        "Conjured Mana Cake, -2, 0" +
        "Conjured Mana Cake, -1, 0";

    String createTextFixture(int days) {

        StringBuffer out = new StringBuffer();
        Item[] items = TexttestFixture.getItems();
        GildedRose app = new GildedRose(items);

        for (int i = 0; i < days; i++) {
            out.append("-------- day " + i + " --------");
            for (Item item : items)
                out.append(item);
            app.updateQuality();
        }
        return out.toString();
    }

    @Test
    void foo() {
        Item[] items = new Item[] { new Item("foo", 0, 0) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertEquals("foo", app.items[0].name);
    }

    @Test
    void testTextFixture() {
        String result = createTextFixture(3);
        assertEquals(GOLDEN_MASTER, result);
    }
}
