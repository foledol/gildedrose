package com.gildedrose;

public class TexttestFixture {

    public static Item[] getItems() {
        return new Item[] {
            new Item("+5 Dexterity Vest", 10, 20), // Quality decrease (x2)
            new Item("+5 Dexterity Vest", 0, 20), // Quality decrease twice as fast
            new Item("+5 Dexterity Vest", 1, 1), // Quality is always greater than 0
            new Item("Aged Brie", 2, 0), // Quality increase (x2)
            new Item("Aged Brie", 2, 49), // Quality is always lower than 50
            // Quality is increasing twice as fast after the selling date (?)
            // This is probably not the expected behavior, to be discussed with the business
            new Item("Aged Brie", 0, 0),
            // Selling date and quality unchanged
            new Item("Sulfuras, Hand of Ragnaros", 0, 80),
            new Item("Sulfuras, Hand of Ragnaros", 20, 40),
            new Item("Sulfuras, Hand of Ragnaros", -1, 40),
            // Quality is always lower than 50
            new Item("Backstage passes to a TAFKAL80ETC concert", 15, 49),
            // Quality increase (x2)
            new Item("Backstage passes to a TAFKAL80ETC concert", 15, 20),
            // Quality increase by 2 (x2)
            new Item("Backstage passes to a TAFKAL80ETC concert", 10, 20),
            // Quality increase by 3 (x2)
            new Item("Backstage passes to a TAFKAL80ETC concert", 5, 20),
            // Quality drops to 0 after the concert
            new Item("Backstage passes to a TAFKAL80ETC concert", 1, 20),
            // Before the selling date, the quality decrease by 2 (twice as fast as normal item)
            new Item("Conjured Mana Cake", 3, 6),
            // After the selling date, the quality decrease by 4 (twice as fast as normal item)
            new Item("Conjured Mana Cake", 0, 4),
            // Quality is always greater than 0
            new Item("Conjured Mana Cake", 1, 1)
        };
    }

    public static void main(String[] args) {

        Item[] items = TexttestFixture.getItems();

        GildedRose app = new GildedRose(items);

        int days = 3;
        if (args.length > 0)
            days = Integer.parseInt(args[0]) + 1;

        for (int i = 0; i < days; i++) {
            System.out.println("-------- day " + i + " --------");
            for (Item item : items)
                System.out.println(item);
            app.updateQuality();
        }
    }

}
