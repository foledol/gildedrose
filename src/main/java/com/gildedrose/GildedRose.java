package com.gildedrose;

class GildedRose {
    Item[] items;

    public static final String AGED_BRIE = "Aged Brie";
    public static final String BACKSTAGE_PASSES = "Backstage passes to a TAFKAL80ETC concert";
    public static final String CONJURED_ITEMS = "Conjured Mana Cake";
    public static final String SULFURAS = "Sulfuras, Hand of Ragnaros";

    public GildedRose(Item[] items) {
        this.items = items;
    }

    private void decreaseQuality(Item item) {
        if (item.quality <= 0) return;
        item.quality--;
        if (item.name.equals(CONJURED_ITEMS) && item.quality > 0)
            item.quality--;
    }

    private void increaseQuality(Item item) {
        if (item.quality >= 50) return;
        item.quality++;
    }

    public void updateQuality() {
        for (Item item : items) {
            if (item.name.equals(SULFURAS))
                continue;

            if (!item.name.equals(AGED_BRIE) && !item.name.equals(BACKSTAGE_PASSES))
                decreaseQuality(item);
            else {
                increaseQuality(item);
                if (item.name.equals(BACKSTAGE_PASSES)) {
                    if (item.sellIn < 11)
                        increaseQuality(item);
                    if (item.sellIn < 6)
                        increaseQuality(item);
                }
            }

            item.sellIn--;

            if (item.sellIn < 0) {
                if (!item.name.equals(AGED_BRIE)) {
                    if (!item.name.equals(BACKSTAGE_PASSES))
                        decreaseQuality(item);
                    else
                        item.quality = 0;
                } else
                    increaseQuality(item);
            }
        }
    }
}
