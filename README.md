# GildedRose

My own resolution of the Gilded Rose Refactoring Kata that can be found [here](https://github.com/emilybache/GildedRose-Refactoring-Kata)

I chose to work with the Java source code.

## Requirements

- Java 1.8.0
- Gradle 5.6.2

## Installation

Clone the git repository

```
git clone git@gitlab.com:foledol/gildedrose.git
```

## Usage

To build the application, run

```
gradle build
```

To test the application, run

```
gradle test
```

## Notes

Exercise performed on Sunday March 15 from 09:00 to 12:30
